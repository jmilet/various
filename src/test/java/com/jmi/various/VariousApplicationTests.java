package com.jmi.various;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class VariousApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ComponentOne componentOne;

	@Test
	void contextLoads() {
	}

	@Test
	public void testContent() throws Exception {
		when(componentOne.foo()).thenReturn("foo");

		final MvcResult mvcResult = this.mockMvc.perform(get("/hello?name=juan")).
				andExpect(status().isOk()).
				andExpect(content().json("{\"name\": \"juan\", \"other\": \"foo\"}")).
				andReturn();
	}

	@Test
	public void shouldReturnDefaultMessage() throws Exception {
		this.mockMvc.perform(get("/hello")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("Hello, World")));
	}
}
