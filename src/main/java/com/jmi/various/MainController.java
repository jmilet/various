package com.jmi.various;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @Autowired
    private ComponentOne componentOne;

    @GetMapping("/hello")
    public Hello hello(@RequestParam(value = "name", defaultValue = "World") final String name) {
        final Hello hello = new Hello();

        hello.setName(name);
        hello.setOther(componentOne.foo());

        return hello;
    }
}
