package com.jmi.various;

import org.springframework.stereotype.Component;

@Component
public class ComponentOne {
    public String foo() {
        return "foo";
    }
}
